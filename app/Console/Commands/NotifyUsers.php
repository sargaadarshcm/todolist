<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Carbon\Carbon;
use Mail;
use Illuminate\Mail\Message;

class NotifyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::with('tasks')->get();
        foreach ($users as $user) {

            $email = $user['email'];

            foreach ($user->tasks as $task) {
                $diffInDays = Carbon::parse($task->duedate)->diff(Carbon::now())->days;
                Mail::raw("Your ".$task->name. " deadline is in " . $diffInDays . " day!", function (Message $message) use ($email) {

                    $message->to($email)
                        ->subject("Reminder")
                        ->from('sargaadarshcm@gmail.com');
                });
            }
        }
    }
}
