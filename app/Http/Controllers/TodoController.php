<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $todos = Todo::select('name', 'priority', 'status', 'duedate', 'id')->where('user_id', Auth::id())->get();
            return Datatables::of($todos)
                ->addColumn('action', function ($row) {
                    if ($row->status == "to-do" | $row->status == "in progress") {
                        $btn = "<a href='/edit/$row->id' data-mdb-toggle='tooltip' title='Edit'><i
                    class='fas fa-check text-success me-3'></i></a>
                    <a data-mdb-toggle='tooltip' title='Remove' class='show_confirm' id=$row->id><i
                    class='fas fa-trash-alt text-danger'></i></a>";
                    } else {
                        $btn = "<a data-mdb-toggle='tooltip' class='show_confirm' title='Remove' id=$row->id><i
                class='fas fa-trash-alt text-danger'></i></a>";
                    }
                    return $btn;
                })
                ->editColumn('priority', function ($todo) {
                    if ($todo->priority == "1") {
                        $priority = "<h6 class='mb-0'><span class='badge bg-danger'>High priority</span></h6>";
                    } else if ($todo->priority == "2") {
                        $priority = "<h6 class='mb-0'><span class='badge bg-warning'>Medium priority</span></h6>";
                    } else {
                        $priority = "<h6 class='mb-0'><span class='badge bg-success'>Low priority</span></h6>";
                    }
                    return $priority;
                })
                ->rawColumns(['action', 'priority'])
                ->make(true);
        }
        return view('index');
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {



        $request->validate([
            'name' => 'required',
            'priority' => 'required',
            'date' => 'required'
        ]);


        $todo = new Todo();
        $todo->user_id = Auth::id();
        $todo->name = $request->name;
        $todo->priority = $request->priority;
        $todo->duedate = $request->date;
        $todo->save();
        session()->flash('success', 'Todo created succesfully');
        return redirect('/index');
    }

    public function details($id)
    {
        $todo = Todo::find($id);
        return view('details')->with('todos', $todo);
    }

    public function edit($id)
    {
        $todo = Todo::find($id);
        return view('edit')->with('todos', $todo);
    }

    public function update(Request $request, $id)
    {



        $request->validate([
            'name' => 'required',
            'priority' => 'required',
            'date' => 'required'
        ]);


        $todo = Todo::find($id);
        $todo->name = $request->name;
        $todo->priority = $request->priority;
        $todo->duedate = $request->date;
        $todo->status = $request->status;
        $todo->save();
        session()->flash('success', 'Todo updated succesfully');
        return redirect('/index');
    }


    public function delete($id)
    {

        $todo = Todo::findOrFail($id);
        $todo->delete();
        return response()->json(['status' => 'Task has been deleted successfully']);
    }
}
