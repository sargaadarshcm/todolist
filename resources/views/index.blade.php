@extends('layouts.todolist')
@section('title')
    Todo App
@endsection
@section('content')

  
            <table class="table yajra-datatable">
                <thead>
                  <tr>
                    <th scope="col">Task</th>
                    <th scope="col">Priority</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
        
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
     
        $(document).ready(function () {
           
            $(function () {
                var table = $('.yajra-datatable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "/index",
                    paging:false,
                    columns: [
                        {data: 'name'},
                        {data: 'priority'},
                        {data: 'status'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

            $(document).on('click', '.show_confirm', function(){
              event.preventDefault();
              var delete_id=$(this).attr('id');
              swal({
                  title: `Are you sure you want to delete this record?`,
                  text: "If you delete this, it will be gone forever.",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                    var data={
                        "_token":'{{ csrf_token() }}',
                        "id":delete_id,
                    };
                    $.ajax({
                        type:"DELETE",
                        url:"/delete/"+delete_id,
                        data:data,
                        success:function (response)
                        {
                            swal(response.status,{
                                icon:"success",
                            })
                            .then((result)=>{
                                location.reload();
                            });
                        }
                    });

                    
                }
              });
          });

        });
      
    </script>
@endsection
