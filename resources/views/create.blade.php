@extends('layouts.todolist')

@section('title')
    Create Todo
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form action="/store" method="post" class="mt-4 p-4">
        @csrf
        <div class="form-group m-3">
            <label for="name">Todo Name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group m-3">
            <label for="priority">Priority</label>
           <select name="priority" class="form-control">
            <option value="">Select</option>
            <option value="1">High</option>
            <option value="2">Medium</option>
            <option value="3">Low</option>
           </select>
        </div>
        <div class="form-group m-3">
            <label for="date">Due Date</label>
            <input type="date" class="form-control" name="date">
        </div>
        <div class="form-group m-3">
            <input type="submit" class="btn btn-primary float-end" value="Submit">
        </div>
    </form>

@endsection