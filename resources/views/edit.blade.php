@extends('layouts.todolist')

@section('title')
    Update Todo
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form action="/update/{{$todos->id}}" method="post" class="mt-4 p-4">
        @csrf
        <div class="form-group m-3">
            <label for="name">Todo Name</label>
            <input type="text" class="form-control" name="name" value="{{ $todos->name }}">
        </div>
        <div class="form-group m-3">
            <label for="priority">Priority</label>
           <select name="priority" class="form-control">
            <option value="">Select</option>
            <option value="1" @if($todos->priority=="1") selected @endif>High</option>
            <option value="2" @if($todos->priority=="2") selected @endif>Medium</option>
            <option value="3" @if($todos->priority=="3") selected @endif>Low</option>
           </select>
        </div>
        <div class="form-group m-3">
            <label for="status">Status</label>
           <select name="status" class="form-control">
            <option @if($todos->status=="in progress") selected @endif>in progress</option>
            <option @if($todos->status=="completed") selected @endif>completed</option>
           </select>
        </div>
        <div class="form-group m-3">
            <label for="date">Due Date</label>
            <input type="date" class="form-control" name="date" value="{{ $todos->duedate }}">
        </div>
        <div class="form-group m-3">
            <input type="submit" class="btn btn-primary float-end" value="Update">
        </div>
    </form>

@endsection